package pl.koszowska.kamila.person;


import pl.koszowska.kamila.elevator.Direction;
import pl.koszowska.kamila.floor.Floor;

/**
 * Created by RENT on 2017-08-21.
 */
public class ElevatorUser extends Person {

    public static final int AVERAGE_WEIGHT_OF_PERSON_KG = 90;

    boolean isInElevator;
    Floor mWantToGoFloor;
    Floor mCurrentFloor;

    public boolean isInElevator() {
        return isInElevator;
    }

    public void setInElevator(boolean inElevator) {
        isInElevator = inElevator;
    }

    public Floor getmWantToGoFloor() {
        return mWantToGoFloor;
    }

    public void setmWantToGoFloor(Floor mWantToGoFloor) {
        this.mWantToGoFloor = mWantToGoFloor;
    }

    public Floor getmCurrentFloor() {
        return mCurrentFloor;
    }

    public void setmCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    public Direction getDirection(){
        return (mWantToGoFloor.getmFloorNo() - mCurrentFloor.getmFloorNo() > 0) ? Direction.UP : Direction.DOWN;
    }
}
