package pl.koszowska.kamila.person;

import pl.koszowska.kamila.address.Address;
import pl.koszowska.kamila.weight.Weight;

/**
 * Created by RENT on 2017-08-23.
 */
public class Person {

    String mFirstName;
    String mName;
    Address mAddress;
    Weight mWeight;

    @Override
    public String toString() {
        return "Nazywam się " + getmFirstName() + " " + getmName() + ". Mieszkam w " + getmAddress();
    }

    public String getmFirstName() {return mFirstName; }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public Address getmAddress() {
        return mAddress;
    }

    public void setmAddress(Address mAddress) {
        this.mAddress = mAddress;
    }

    public Weight getmWeight() {
        return mWeight;
    }

    public void setmWeight(Weight mWeight) {
        this.mWeight = mWeight;
    }
}
