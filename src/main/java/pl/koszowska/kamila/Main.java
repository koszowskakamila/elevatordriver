package pl.koszowska.kamila;

import pl.koszowska.kamila.address.Address;
import pl.koszowska.kamila.address.City;
import pl.koszowska.kamila.address.utils.PostalCodeUtils;
import pl.koszowska.kamila.block.BlockWithElevators;
import pl.koszowska.kamila.elevator.Elevator;
import pl.koszowska.kamila.elevator.PassangerElevator;
import pl.koszowska.kamila.floor.Floor;
import pl.koszowska.kamila.person.ElevatorUser;
import pl.koszowska.kamila.person.Person;
import pl.koszowska.kamila.weight.Weight;
import pl.koszowska.kamila.weight.WeightMetrics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by RENT on 2017-08-23.
 */
public class Main {
    public static void main(String[] args) {
        Person person = new Person();
        person.setmFirstName("Kamila");
        person.setmName("Koszowska");
        person.setmWeight(new Weight(70, WeightMetrics.KG));
        Address address = new Address(City.LUBLIN, "Dunikowskiego");
        address.setmBlockNo(1);
        address.setmApartmentNo(2);
        address.setmPostalCode("21-132");
        person.setmAddress(address);

        System.out.println(person);

        PostalCodeUtils.isCorrect("21-132", "POL");

        List<Floor> floors = new ArrayList<>(12);
        for (int i=-2;i<12;i++){
            floors.add(new Floor(i));
        }



/*
        List<Elevator> elevator = new ArrayList<>();
        List<ElevatorUser> user = new ArrayList<>();
        HashMap<Floor, List<ElevatorUser>> mapa = new HashMap<>();

*/
        BlockWithElevators block = new BlockWithElevators(floors);
        block.setmAddress(address);

        Elevator elevator = new PassangerElevator();
        elevator.setmAvailableFloors(block.getmFloor());
        elevator.setmCurrentFloor(new Floor(0));
        elevator.setmMaxLoad(new Weight(800, WeightMetrics.KG));
        block.setmElevators(Arrays.asList(elevator));

        ElevatorUser elevatorUser = new ElevatorUser();
        ElevatorUser elevatorUser1 = new ElevatorUser();

 //       Floor floor = new Floor(2);
        block.addElevatorUserToQueue(new Floor(1), elevatorUser);
        block.addElevatorUserToQueue(new Floor(1), elevatorUser1);

        Scanner sc = new Scanner(System.in);
        String name = "";
        int currentF = 0;
        int destinationF = 0;
        String exit;
        String temp;

        do {

            System.out.println("Podaj imię:");
            name = sc.nextLine();
            System.out.println("Podaj obecne piętro:");
            temp = sc.nextLine();
            currentF = Integer.valueOf(temp);
            System.out.println("Podaj docelowe piętro:");
            temp = sc.nextLine();
            destinationF = Integer.valueOf(temp);

            ElevatorUser elevatorU = new ElevatorUser();
            elevatorU.setmFirstName(name);
            elevatorU.setmCurrentFloor(new Floor(currentF));
            elevatorU.setmWantToGoFloor(new Floor(destinationF));

            block.addElevatorUserToQueue(new Floor(destinationF), elevatorU);

            System.out.println("Czy chcesz dodać klejną osobę, jeśli nie wpisz 'exit'");
            exit = sc.nextLine();


        } while(!exit.equals("exit"));

    }
}
