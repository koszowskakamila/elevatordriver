package pl.koszowska.kamila.weight;

/**
 * Created by RENT on 2017-08-21.
 */
public class Weight {

    private int mAmount;
    private WeightMetrics mWeightMetrics;
    public static final float LB_TO_KG = 2.2046f;

    public Weight(int mAmount, WeightMetrics mWeightMetrics) {
        this.mAmount = mAmount;
        this.mWeightMetrics = mWeightMetrics;
    }

    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public WeightMetrics getmWeightMetrics() {
        return mWeightMetrics;
    }

    public void setmWeightMetrics(WeightMetrics mWeightMetrics) {
        this.mWeightMetrics = mWeightMetrics;
    }
}
