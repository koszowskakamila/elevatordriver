package pl.koszowska.kamila.block;

import pl.koszowska.kamila.address.Address;
import pl.koszowska.kamila.floor.Floor;

import java.util.List;

/**
 * Created by RENT on 2017-08-23.
 */
public abstract class Block {

    private List<Floor> mFloor;
    private Address mAddress;

    public Block(List<Floor> mFloor) {
        this.mFloor = mFloor;
    }

    public abstract boolean hasElevator();

    public List<Floor> getmFloor() {
        return mFloor;
    }

    public void setmFloor(List<Floor> mFloor) {
        this.mFloor = mFloor;
    }

    public Address getmAddress() {
        return mAddress;
    }

    public void setmAddress(Address mAddress) {
        this.mAddress = mAddress;
    }
}
