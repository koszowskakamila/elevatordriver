package pl.koszowska.kamila.block;

import pl.koszowska.kamila.floor.Floor;
import pl.koszowska.kamila.person.ElevatorUser;

import java.util.List;

public interface QueueChecker {

    boolean isAnyQueueFloor(Floor aFloor);
    List<ElevatorUser> getPersonOnFloor(Floor aFloor);
    void removePersonFromQueue(ElevatorUser elevatorUser);

}
