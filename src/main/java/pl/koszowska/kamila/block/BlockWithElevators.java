package pl.koszowska.kamila.block;

import pl.koszowska.kamila.elevator.Elevator;
import pl.koszowska.kamila.floor.Floor;
import pl.koszowska.kamila.person.ElevatorUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by RENT on 2017-08-23.
 */
public class BlockWithElevators extends Block implements QueueChecker{

    private List<Elevator> mElevators;
    private HashMap<Floor, List<ElevatorUser>> mElevatorQueue;

    public BlockWithElevators(List<Floor> mFloor) {
        super(mFloor);
        mElevatorQueue = new HashMap<>();
    }

    public List<Elevator> getmElevators() {
        return mElevators;
    }

    public void setmElevators(List<Elevator> mElevators) {
        this.mElevators = mElevators;
    }

    @Override
    public boolean hasElevator() { return true;}

    public void addElevatorUserToQueue(Floor aFloor, ElevatorUser aElevatorUser){
        List<ElevatorUser> listOfQueueOnFloor = mElevatorQueue.get(aFloor);

        if (listOfQueueOnFloor!= null){
            listOfQueueOnFloor.add(aElevatorUser);
            System.out.println("Dodajemy " + aElevatorUser.getmName() + " do listy na piętro " + aFloor);
        }else {
            ArrayList<ElevatorUser> elevatorUsers = new ArrayList<>();
            elevatorUsers.add(aElevatorUser);
            mElevatorQueue.put(aFloor, elevatorUsers);
            System.out.println("Nie mamy oczekujących na wjazd na piętro " + aFloor + ". Tworzymy listę oczekujących z osobą " + elevatorUsers.get(0).getmName());
        }
    }

    @Override
    public boolean isAnyQueueFloor(Floor aFloor) {
        return mElevatorQueue.containsKey(aFloor);
    }

    @Override
    public List<ElevatorUser> getPersonOnFloor(Floor aFloor) {
        return mElevatorQueue.get(aFloor);
    }

    @Override
    public void removePersonFromQueue(ElevatorUser elevatorUser) {
        mElevatorQueue.get(elevatorUser.getmCurrentFloor()).remove(elevatorUser);
    }
}
