package pl.koszowska.kamila.address.utils;

import com.sun.istack.internal.Nullable;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by RENT on 2017-08-23.
 */
final public class PostalCodeUtils {

    private PostalCodeUtils() {
        // do nothing
    }

    public static boolean isCorrect(String aCode, @Nullable String aRegion){

        boolean result = false;

        if (aRegion == null) {
            Locale locale = Locale.getDefault();
            aRegion = locale.getISO3Country();
        }

        Pattern pattern = null;

        switch (aRegion){
            case "POL":
                pattern = Pattern.compile("\\d\\d-\\d\\d\\d");
                break;
            case "USA":
                pattern = Pattern.compile("\\d{5}");
                break;

        }

        if (pattern != null) {
            Matcher matcher = pattern.matcher(aCode);
            result = matcher.matches();
        }

        return result;
    }

}
