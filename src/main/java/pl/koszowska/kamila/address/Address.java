package pl.koszowska.kamila.address;

/**
 * Created by RENT on 2017-08-23.
 */
final public class Address {

    private  City mCity;
    private  String mStreetName;
    private  int mBlockNo;
    private int mApartmentNo;
    private String mPostalCode;

    public Address(City mCity, String mStreetName) {
        this.mCity = mCity;
        this.mStreetName = mStreetName;
    }

    @Override
    public String toString() {
        return getmCity() + " na ul. " + getmStreetName() + " " + getmBlockNo();
    }

    public City getmCity() {
        return mCity;
    }

    public void setmCity(City mCity) {
        this.mCity = mCity;
    }

    public String getmStreetName() {
        return mStreetName;
    }

    public void setmStreetName(String mStreetName) {
        this.mStreetName = mStreetName;
    }

    public int getmBlockNo() {
        return mBlockNo;
    }

    public void setmBlockNo(int mBlockNo) {
        this.mBlockNo = mBlockNo;
    }

    public int getmApartmentNo() {
        return mApartmentNo;
    }

    public void setmApartmentNo(int mApartmentNo) {
        this.mApartmentNo = mApartmentNo;
    }

    public String getmPostalCode() {
        return mPostalCode;
    }

    public void setmPostalCode(String mPostalCode) {
        this.mPostalCode = mPostalCode;
    }
}
