package pl.koszowska.kamila.address;

/**
 * Created by RENT on 2017-08-23.
 */
public enum City {

    LUBLIN ("Lublin"), WARSZAWA("Warszawa"), STALOWA_WOLA("Stalowa Wola"), POZNAN("Poznań"), LODZ("Łódź");

    String mCityName;

    City(String aCityName) {
        mCityName = aCityName;
    }

    public String getCityName() {
        return mCityName;
    }

    @Override
    public String toString() {
        return mCityName;
    }

}
