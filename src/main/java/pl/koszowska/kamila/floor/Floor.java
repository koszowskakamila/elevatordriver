package pl.koszowska.kamila.floor;

/**
 * Created by RENT on 2017-08-21.
 */
public class Floor {
    private int mFloorNo;

    public Floor(int mFloorNo) {
        this.mFloorNo = mFloorNo;
    }

    @Override
    public String toString() {
        return "Floor{" +
                "mFloorNo=" + mFloorNo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Floor)) return false;

        Floor floor = (Floor) o;

        return getmFloorNo() == floor.getmFloorNo();

    }

    @Override
    public int hashCode() {
        return Integer.hashCode(mFloorNo);
    }

    public int getmFloorNo() {
        return mFloorNo;
    }
    public void setmFloorNo(int mFloorNo) {
        this.mFloorNo = mFloorNo;
    }

    public void upFloor(){
        mFloorNo++;
    }

    public void downFloor(){ mFloorNo--; }
}
