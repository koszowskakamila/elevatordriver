package pl.koszowska.kamila.elevator;

import pl.koszowska.kamila.block.BlockWithElevators;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by RENT on 2017-08-23.
 */
public class ElevatorManager {
    private BlockWithElevators blockWithElevators;
    AtomicBoolean isWorking = new AtomicBoolean(false);

    public ElevatorManager(BlockWithElevators blockWithElevators) {
        this.blockWithElevators = blockWithElevators;
    }
    
    public boolean startWork(){
        
        if (isWorking.get()){
            return false;
        }
        
        isWorking.set(true);
        
        new Thread(() -> {while (isWorking.get()) {
            moveElevators();
            printState();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }}).start(); 
        
        return true;
    }

    private void moveElevators() {
        blockWithElevators.getmElevators().stream().forEach(elevator -> elevator.nextStep());
    }

    private void printState() {
        blockWithElevators.getmElevators().stream().forEach(elevator -> elevator.toString());
    }


}
