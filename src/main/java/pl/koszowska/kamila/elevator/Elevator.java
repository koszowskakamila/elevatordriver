package pl.koszowska.kamila.elevator;

import pl.koszowska.kamila.block.QueueChecker;
import pl.koszowska.kamila.floor.Floor;
import pl.koszowska.kamila.weight.Weight;

import java.util.List;

/**
 * Created by RENT on 2017-08-21.
 */
public class Elevator {

    public static final float RESERVE = 0.9f;
    QueueChecker queueChecker;

    private List<Floor> mAvailableFloors;
    private List<Floor> mRequestFloors;
    private Floor mCurrentFloor;
    private boolean mServiceBreak;
    private Weight mMaxLoad;
    private Direction mDirection;

    public List<Floor> getmAvailableFloors() {
        return mAvailableFloors;
    }

    public void setmAvailableFloors(List<Floor> mAvailableFloors) {
        this.mAvailableFloors = mAvailableFloors;
    }

    public List<Floor> getmRequestFloors() {
        return mRequestFloors;
    }

    public void setmRequestFloors(List<Floor> mRequestFloors) {
        this.mRequestFloors = mRequestFloors;
    }

    public Floor getmCurrentFloor() {
        return mCurrentFloor;
    }

    public void setmCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    public boolean ismServiceBreak() {
        return mServiceBreak;
    }

    public void setmServiceBreak(boolean mServiceBreak) {
        this.mServiceBreak = mServiceBreak;
    }

    public Weight getmMaxLoad() {
        return mMaxLoad;
    }

    public void setmMaxLoad(Weight mMaxLoad) {
        this.mMaxLoad = mMaxLoad;
    }

    public Direction getmDirection() {
        return mDirection;
    }

    public void setmDirection(Direction mDirection) {
        this.mDirection = mDirection;
    }

    @Override
    public String toString() {
        return super.toString() + " jestem na piętrze " + mCurrentFloor + ". Jadę " + mDirection;
    }

    public void nextStep() {
        switch (mDirection){
            case UP:
                mCurrentFloor.upFloor();
                break;
            case DOWN:
                mCurrentFloor.downFloor();
                break;
        }
    }
}
