package pl.koszowska.kamila.elevator;

import pl.koszowska.kamila.person.ElevatorUser;
import pl.koszowska.kamila.weight.Weight;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by RENT on 2017-08-21.
 */
public class PassangerElevator extends Elevator {

    private List<ElevatorUser> mPersonsOnBoard = new ArrayList<>();
    private short mMaxLoadPerson;

    @Override
    public void setmMaxLoad(Weight mMaxLoad) {
        super.setmMaxLoad(mMaxLoad);
        float maxLP = (getmMaxLoad().getmAmount() * RESERVE);
        float typicalPersolWeight = ElevatorUser.AVERAGE_WEIGHT_OF_PERSON_KG;

        switch (getmMaxLoad().getmWeightMetrics()){
            case KG: break;
            case LB: typicalPersolWeight *= Weight.LB_TO_KG; break;
        }

        mMaxLoadPerson = (short) (maxLP / typicalPersolWeight);
    }

    @Override
    public void nextStep() {
        mPersonsOnBoard.stream()
                .filter(user -> user.getmWantToGoFloor().equals(getmCurrentFloor()))
                .forEach(user ->{
                    System.out.println(user + " wyszedł z windy na piętrze "+ getmCurrentFloor());
                    mPersonsOnBoard.remove(user);
                });

        if (mPersonsOnBoard.size() == 0){
            setmDirection(Direction.NONE);
        }

        if (queueChecker.isAnyQueueFloor(getmCurrentFloor())){
            ArrayList<ElevatorUser> tmpList = (ArrayList<ElevatorUser>) queueChecker.getPersonOnFloor(getmCurrentFloor()).stream()
                    .filter(user -> user.getDirection().equals(getmDirection())).collect(Collectors.toList());

            if (getmDirection() == Direction.NONE){
                setmDirection(tmpList.get(0).getDirection());
            }

            mPersonsOnBoard.addAll(tmpList);
            tmpList.stream().forEach(user -> queueChecker.removePersonFromQueue(user));
        }

        super.nextStep();
    }
}
